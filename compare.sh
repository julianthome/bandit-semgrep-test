#!/usr/bin/env bash

REPORT="$1"
RULE="$2"

[ -z "$REPORT" ] && {
    echo "no report name specified"
    exit 1
}

[ -f "$REPORT" ] && mv "$REPORT" "$REPORT-$(date +%s)"

source ./env/bin/activate
./compare.rb bandit,semgrep "$RULE" >> "$REPORT"

exit 0
