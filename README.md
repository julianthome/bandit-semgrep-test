# Rule coverage test semgrep bandit rule-set <> bandit (baseline)

This is a small project that helps analyzing the rule coverage of the [semgrep
bandit ruleset](https://semgrep.dev/c/p/bandit) (State: 2021/02/03) in
comparison to bandit v1.7.0 as our baseline.

In this repository you can find the bandit patterns in separate directories
with the [bandit rule ids `BXX`](https://github.com/PyCQA/bandit#baseline).
Every folder contains a python file that contains patterns that are expected to
be found by bandit. The test-cases are based on the [bandit examples](https://github.com/PyCQA/bandit/tree/master/examples)

You can find a helper script in this repository that helps with the coverage
analysis. For example, when running `make RULE=B101`, the script generates the
output below and writes it to `results.txt`. The first table shows the findings
for `bandit` and `semgrep` where an `X` indicates which tool reported the finding; 
an `-` indicates that a tool did not report a finding.

For the purpose of our coverage analysis, we wanted to make sure that whenever
whenever `bandit` has an `X`, `semgrep` with the [semgrep bandit ruleset](https://semgrep.dev/c/p/bandit)
should have an `X` too. The second table shows more detailed results about the
findings and the third table shows some performance measures.

``` bash
########## Comparing rule: B101 #####################
+-----------------------+--------+---------+
| rule                  | bandit | semgrep |
+-----------------------+--------+---------+
| B101/assert_used.py:3 | X      | -       |
+-----------------------+--------+---------+
+--------+------+---------------------+------+-----------------------+
| tag    | rule | path                | line | hash                  |
+--------+------+---------------------+------+-----------------------+
| bandit | B101 | B101/assert_used.py | 3    | B101/assert_used.py:3 |
+--------+------+---------------------+------+-----------------------+
+------------------------------+--------------------+
| tag                          | execution time (s) |
+------------------------------+--------------------+
| bandit                       | 0.118981686        |
| semgrep-bandit-original.yaml | 5.143310659        |
+------------------------------+--------------------+
########################################################
```

In case you would like to run a full test on all bandit patterns, you can run
`make`. You can find a full report in the `report.txt` files afterwards.
