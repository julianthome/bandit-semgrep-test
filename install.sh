#!/usr/bin/env bash

[ -d "env" ] && exit 0

gem install open3 json time terminal-table
python3 -m venv env
source "env/bin/activate"
pip install bandit
pip install semgrep

exit 0
