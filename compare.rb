#!/usr/bin/env ruby
# frozen_string_literal: true

require 'open3'
require 'json'
require 'time'
require 'terminal-table'

USAGE = './compare.rb <comma-separated analyzer bandit|semprep> <rule|all>'
SEMGREP_RULES_FILES = ['bandit-original.yaml'].freeze

if ARGV.count != 2
  puts USAGE
  exit(1)
end

def semgrep_cmds(rule)
  SEMGREP_RULES_FILES.map { |cf| ["semgrep-#{cf}", "semgrep --json -q --config #{cf} #{rule}"] }.to_h
end

def bandit_cmds(rule)
  { 'bandit' => "bandit -f json -r #{rule}" }
end

rules = Dir.glob('B*').map { |b| b[1..-1].to_i }.sort.map { |b| "B#{b}" }

tools = ARGV[0]
rulearg = ARGV[1]
cmds = {}

rules = rulearg == 'all' ? rules : [rulearg]

rules.each do |rule|

  puts "########## Comparing rule: #{rule} #####################"
  rows = {}
  exectime = []
  
  tools.split(',').each do |tool|
    case tool
    when 'semgrep'
      cmds.merge!(semgrep_cmds(rule))
    when 'bandit'
      cmds.merge!(bandit_cmds(rule))
    when 'semgrep+bandit'
      cmds.merge!(bandit_cmds(rule))
      cmds.merge!(semgrep_cmds(rule))
    else
      puts USAGE
      exit(1)
    end
  end

  cmds.each do |tag, cmd|
    start_t = Time.now
    Open3.popen3(cmd) do |_stdin, stdout, _stderr, thread|
      pid = thread.pid
      out = JSON.parse(stdout.read.chomp)
      duration = Time.now - start_t
      if tag.include?('semgrep')
        exectime << [tag, duration]
        rows['semgrep'] = [] unless rows.key?('semgrep')
        out['results'].each do |r|
          rows['semgrep'] << [tag, r['check_id'], r['path'], r['start']['line'], "#{r['path']}:#{r['start']['line']}"]
        end
      elsif tag.include?('bandit')
        exectime << [tag, duration]
        rows['bandit'] = [] unless rows.key?('semgrep')
        out['results'].each do |r|
          next if r['test_id'] != rule

          rows['bandit'] << [tag, r['test_id'], r['filename'], r['line_number'],
                             "#{r['filename']}:#{r['line_number']}"]
        end
      else
        puts 'invalid tags'
        exit(1)
      end
    end
  end

  if rows.key?('semgrep') && rows.key?('bandit')
    semgrep_hashes = rows['semgrep'].map { |r| r[4] }
    bandit_hashes = rows['bandit'].map { |r| r[4] }

    common_hashes = semgrep_hashes + bandit_hashes
    common_hashes.sort!
    common_hashes.uniq!

    overlap = []
    common_hashes.each do |ch|
      overlap << [ch, bandit_hashes.include?(ch) ? 'X' : '-', semgrep_hashes.include?(ch) ? 'X' : '-']
    end

    puts Terminal::Table.new(rows: overlap, headings: %w[rule bandit semgrep])
  end

  puts Terminal::Table.new(rows: rows.flat_map { |_k, v| v }, headings: %w[tag rule path line hash])
  puts Terminal::Table.new(rows: exectime, headings: ['tag', 'execution time (s)'])

  puts "########################################################"
end

exit 0
